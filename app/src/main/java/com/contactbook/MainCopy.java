package com.contactbook;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.CallLog;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Event;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

public class MainCopy extends AppCompatActivity {
    RecyclerView mRecyclerView;
    List<Contact> feed_list= new ArrayList<Contact>();
    private RecyclerView.Adapter mrAdapter;
    private ProgressBar progressBar;
    boolean busy=false;
    boolean hasLogPermission=false;
    boolean hasContactPermission=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mRecyclerView = (RecyclerView) findViewById(R.id.rv_contact);
        progressBar= (ProgressBar) findViewById(R.id.progressBar);

        //StaggeredGridLayoutManager staggeredGrid= new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);

        LinearLayoutManager llm= new LinearLayoutManager(getBaseContext());
        mRecyclerView.setLayoutManager(llm);
        mrAdapter = new RVadapter(this,feed_list);

        mRecyclerView.setAdapter(mrAdapter);
        mRecyclerView.setHasFixedSize(true);
        SpacesItemDecoration decoration = new SpacesItemDecoration(10);
        mRecyclerView.addItemDecoration(decoration);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Snackbar.make(view, "", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                //test51();
                //Dexter.initialize(getApplicationContext());
                //Dexter.checkPermission(listener,Manifest.permission.READ_CONTACTS);
                go();
                //goes to new async2()
                Log.d("ss","ddfds");
            }
        });
        //go();

    }

    //@Override
   // public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
   //     getMenuInflater().inflate(R.menu.menu_main, menu);
    //    return true;
    //}

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    public ArrayList<String> getNameEmailDetails(){
        ArrayList<String> names = new ArrayList<String>();
        ContentResolver cr = getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,null, null, null, null);
        if (cur.getCount() > 0) {
            while (cur.moveToNext()) {
                String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
                Cursor cur1 = cr.query(
                        ContactsContract.CommonDataKinds.Email.CONTENT_URI, null,
                        ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?",
                        new String[]{id}, null);
                while (cur1.moveToNext()) {
                    //to get the contact names
                    String name=cur1.getString(cur1.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                    Log.e("Name :", name);
                    String email = cur1.getString(cur1.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                    Log.e("Email", email);
                    if(email!=null){
                        names.add(name);
                    }
                }
                cur1.close();
            }
        }
        return names;
    }
    public ArrayList<String> getNameEmailDetails2() {
        ArrayList<String> emlRecs = new ArrayList<String>();
        HashSet<String> emlRecsHS = new HashSet<String>();
        Context context = getApplicationContext();
        ContentResolver cr = context.getContentResolver();
        String[] PROJECTION = new String[] { ContactsContract.RawContacts._ID,
                ContactsContract.Contacts.DISPLAY_NAME,
                ContactsContract.Contacts.PHOTO_ID,
                ContactsContract.CommonDataKinds.Email.DATA,
                ContactsContract.CommonDataKinds.Photo.CONTACT_ID,
                ContactsContract.Contacts.HAS_PHONE_NUMBER,
                ContactsContract.Contacts.LAST_TIME_CONTACTED,
                ContactsContract.Contacts.PHOTO_URI,
                ContactsContract.CommonDataKinds.Phone.NUMBER,
                ContactsContract.Data.CONTACT_ID};
        String order = "CASE WHEN "
                + ContactsContract.Contacts.DISPLAY_NAME
                + " NOT LIKE '%@%' THEN 1 ELSE 2 END, "
                + ContactsContract.Contacts.DISPLAY_NAME
                + ", "
                + ContactsContract.CommonDataKinds.Email.DATA
                + " COLLATE NOCASE";
        String filter = ContactsContract.CommonDataKinds.Email.DATA + " NOT LIKE ''";
        Cursor cur = cr.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, PROJECTION, filter, null, order);
        if (cur.moveToFirst()) {
            do {

                // names comes in hand sometimes
                String name = cur.getString(1);
                String emlAddr = cur.getString(3);
                // keep unique only
                //get phone number
                DatabaseUtils.dumpCurrentRow(cur);
            } while (cur.moveToNext());
        }

        cur.close();
        return emlRecs;
    }
    private void test(){
        ContentResolver cr = getContentResolver();
        Cursor cur = cr.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI,
                null, null, null, null);
        if (cur.getCount() > 0) {
            while (cur.moveToNext()) {

                String id = cur.getString(
                        cur.getColumnIndex(ContactsContract.CommonDataKinds.Email.CONTACT_ID));
                //get phone number
                String phoneNumber=null;
                Cursor phoneCursor = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[] { id+"" }, null);
                while (phoneCursor.moveToNext()) {
                    phoneNumber = phoneCursor.getString(phoneCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                    System.out.println("## "+phoneNumber);
                    DatabaseUtils.dumpCurrentRow(cur);
                }
                phoneCursor.close();




                String name = cur.getString(
                        cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                if (Integer.parseInt(cur.getString(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                    //Query phone here.  Covered next
                }
            }
        }
    }
    private void test2(){
        ContentResolver resolver = getContentResolver();
        Cursor c = resolver.query(
                ContactsContract.Data.CONTENT_URI,
                null,
                ContactsContract.Data.HAS_PHONE_NUMBER + "!=0 AND (" + ContactsContract.Data.MIMETYPE + "=? OR " + ContactsContract.Data.MIMETYPE + "=?)",
                new String[]{ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE, ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE},
                ContactsContract.Data.CONTACT_ID);

        while (c.moveToNext()) {
            long id = c.getLong(c.getColumnIndex(ContactsContract.Data.CONTACT_ID));
            String name = c.getString(c.getColumnIndex(ContactsContract.Data.DISPLAY_NAME));
            String data1 = c.getString(c.getColumnIndex(ContactsContract.Data.DATA1));
            //String a = c.getString(c.getColumnIndex(ContactsContract.Data.DATA4));
            //String b = c.getString(c.getColumnIndex(ContactsContract.Data.DATA5));
            String lastTimeContact = c.getString(c.getColumnIndex(ContactsContract.Data.LAST_TIME_CONTACTED));

            //get phone number
            String phoneNumber=null;
            Cursor phoneCursor = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[] { id+"" }, null);
            while (phoneCursor.moveToNext()) {
                phoneNumber = phoneCursor.getString(phoneCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            }
            phoneCursor.close();
            System.out.println(id + ", name=" + name + ", #1=" + data1+ ", #lasttime=" + lastTimeContact+" #3="+phoneNumber);
        }
    }
    public void test4() {
        //contactList = new ArrayList<String>();
        String phoneNumber = null;
        String email = null;

        Uri CONTENT_URI = ContactsContract.Contacts.CONTENT_URI;

        String _ID = ContactsContract.Contacts._ID;
        String DISPLAY_NAME = ContactsContract.Contacts.DISPLAY_NAME;
        String LAST_TIME_CONTACTED = ContactsContract.Contacts.LAST_TIME_CONTACTED;

        String HAS_PHONE_NUMBER = ContactsContract.Contacts.HAS_PHONE_NUMBER;

        Uri PhoneCONTENT_URI = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        String Phone_CONTACT_ID = ContactsContract.CommonDataKinds.Phone.CONTACT_ID;
        String NUMBER = ContactsContract.CommonDataKinds.Phone.NUMBER;
        Uri EmailCONTENT_URI =  ContactsContract.CommonDataKinds.Email.CONTENT_URI;
        String EmailCONTACT_ID = ContactsContract.CommonDataKinds.Email.CONTACT_ID;
        String DATA = ContactsContract.CommonDataKinds.Email.DATA;

        ContentResolver contentResolver = getContentResolver();
        Cursor cursor = contentResolver.query(CONTENT_URI, null,ContactsContract.Contacts.HAS_PHONE_NUMBER + " !=0", null, null);
        // Iterate every contact in the phone
        if (cursor.getCount() > 0) {

            while (cursor.moveToNext()) {


                String contact_id = cursor.getString(cursor.getColumnIndex( _ID )); //CONTACT ID

                String name = cursor.getString(cursor.getColumnIndex( DISPLAY_NAME )); //print name
                String lastContactTime=cursor.getString(cursor.getColumnIndex( LAST_TIME_CONTACTED )); // last contact time in millis

                int hasPhoneNumber = Integer.parseInt(cursor.getString(cursor.getColumnIndex( HAS_PHONE_NUMBER )));
                if (hasPhoneNumber > 0) {

                    phoneNumber=null;
                    StringBuilder sb= new StringBuilder();
                    //This is to read multiple phone numbers associated with the same contact
                    Cursor phoneCursor = contentResolver.query(PhoneCONTENT_URI, null, Phone_CONTACT_ID + " = ?", new String[] { contact_id }, null);

                    while (phoneCursor.moveToNext()) {
                        phoneNumber = phoneCursor.getString(phoneCursor.getColumnIndex(NUMBER));
                        sb.append(" \n ");
                        sb.append(phoneNumber);
                        //output.append("\n Phone number:" + phoneNumber);
                    }


                    email=null;
                    Cursor emailCursor = contentResolver.query(EmailCONTENT_URI,    null, EmailCONTACT_ID+ " = ?", new String[] { contact_id }, null);
                    while (emailCursor.moveToNext()) {
                        email = emailCursor.getString(emailCursor.getColumnIndex(DATA));
                        //output.append("\n Email:" + email);
                    }


                    if(email!=null){

                        System.out.println("#1="+name+"#2=" + sb.toString() + ", #3=" + email);

                        //DatabaseUtils.dumpCurrentRow(cursor);
                        //System.out.println("#####################################################");

                        Contact contact=new Contact();

                        contact.setName(name);
                        contact.setMobile(sb.toString());
                        contact.setEmail(email);
                        contact.setId(contact_id);
                        contact.setLastContactTime(lastContactTime);

                        feed_list.add(contact);
                        mrAdapter.notifyDataSetChanged();
                    }
                    else{
                    }
                    phoneCursor.close();
                    emailCursor.close();
                    // Read every email id associated with the contact
                }
                // Add the contact to the ArrayList
                //contactList.add(output.toString());
            }
            // ListView has to be updated using a ui thread
            // Dismiss the progressbar after 500 millisecondds
        }
    }

    public List<Contact> test5(){
        ArrayList<Contact> list=new ArrayList<Contact>();

        Uri CONTENT_URI = ContactsContract.CommonDataKinds.Email.CONTENT_URI;

        String EMAIL_CONTACT_ID = ContactsContract.CommonDataKinds.Email.CONTACT_ID;
        String EMAIL_STRING = ContactsContract.CommonDataKinds.Email.DATA;
        String DISPLAY_NAME = ContactsContract.CommonDataKinds.Email.DISPLAY_NAME_PRIMARY;
        String LAST_CONTACT_TIME=ContactsContract.CommonDataKinds.Email.LAST_TIME_CONTACTED;
        String PHOTO_URI = ContactsContract.CommonDataKinds.Email.PHOTO_URI;
        String PHOTO_URI_THUMB = ContactsContract.CommonDataKinds.Email.PHOTO_THUMBNAIL_URI;

        Uri PhoneCONTENT_URI = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        String Phone_CONTACT_ID = ContactsContract.CommonDataKinds.Phone.CONTACT_ID;
        String NUMBER = ContactsContract.CommonDataKinds.Phone.NUMBER;
        String PHONE_TYPE = ContactsContract.CommonDataKinds.Phone.TYPE;

        ContentResolver contentResolver = getContentResolver();
        Cursor cursor = contentResolver.query(CONTENT_URI, null,ContactsContract.CommonDataKinds.Email.HAS_PHONE_NUMBER + " !=0", null, null);
        int count=0;
        String phoneNumber;
        while (cursor.moveToNext()){
            count=count+1;
            System.out.println(count);
            String email_contact_id=cursor.getString(cursor.getColumnIndex(EMAIL_CONTACT_ID));
            String email=cursor.getString(cursor.getColumnIndex(EMAIL_STRING));
            String name=cursor.getString(cursor.getColumnIndex(DISPLAY_NAME));
            String lastContact=cursor.getString(cursor.getColumnIndex(LAST_CONTACT_TIME));
            String photo_uri=cursor.getString(cursor.getColumnIndex(PHOTO_URI));
            String photo_uri_thumb=cursor.getString(cursor.getColumnIndex(PHOTO_URI_THUMB));

            //DatabaseUtils.dumpCurrentRow(cursor);

            phoneNumber=null;
            String type=null;
            StringBuilder sb= new StringBuilder();
            //This is to read multiple phone numbers associated with the same contact
            Cursor phoneCursor = contentResolver.query(PhoneCONTENT_URI, null, Phone_CONTACT_ID + " = ?", new String[] { email_contact_id }, null);

            while (phoneCursor.moveToNext()) {
                phoneNumber = phoneCursor.getString(phoneCursor.getColumnIndex(NUMBER));
                //type = phoneCursor.getString(phoneCursor.getColumnIndex(PHONE_TYPE));
                DatabaseUtils.dumpCurrentRow(phoneCursor);
                sb.append(" \n ");
               /* String type_text=" -null- ";
                switch (Integer.parseInt(type)){
                    case ContactsContract.CommonDataKinds.Phone.TYPE_HOME:
                        System.out.println("home");
                        type_text="home";
                        break;
                    case ContactsContract.CommonDataKinds.Phone.TYPE_WORK:
                        System.out.println("work");
                        type_text="work";
                        break;
                    case ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE:
                        System.out.println("mobile");
                        type_text="mobile";
                        break;
                    default:
                        break;
                }*/
                sb.append(phoneNumber);
                //output.append("\n Phone number:" + phoneNumber);
            }

            Contact contact= new Contact();
            contact.setName(name);
            contact.setId(email_contact_id);
            contact.setEmail(email);
            contact.setLastContactTime(lastContact);
            contact.setMobile(sb.toString());
            if (photo_uri!=null){
                contact.setPhoto_url(Uri.parse(photo_uri));
            }
            if(photo_uri_thumb!=null){
                contact.setPhoto_url_thumb(Uri.parse(photo_uri_thumb));
            }

            list.add(contact);
            //mrAdapter.notifyDataSetChanged();
            //System.out.println(sb.toString());

        }
        return list;
    }
    public List<Contact> test51(){
        ArrayList<Contact> list=new ArrayList<Contact>();

        Uri CONTENT_URI = ContactsContract.CommonDataKinds.Email.CONTENT_URI;

        String EMAIL_CONTACT_ID = ContactsContract.CommonDataKinds.Email.CONTACT_ID; //0
        String EMAIL_STRING = ContactsContract.CommonDataKinds.Email.DATA;//1
        String DISPLAY_NAME = ContactsContract.CommonDataKinds.Email.DISPLAY_NAME_PRIMARY;//2
        String LAST_CONTACT_TIME=ContactsContract.CommonDataKinds.Email.LAST_TIME_CONTACTED;//3
        String PHOTO_URI = ContactsContract.CommonDataKinds.Email.PHOTO_URI;//4
        String PHOTO_URI_THUMB = ContactsContract.CommonDataKinds.Email.PHOTO_THUMBNAIL_URI;//5

        String [] PROJECTION_EMAIL={EMAIL_CONTACT_ID,EMAIL_STRING,DISPLAY_NAME,LAST_CONTACT_TIME,PHOTO_URI,PHOTO_URI_THUMB};

        Uri PhoneCONTENT_URI = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        String Phone_CONTACT_ID = ContactsContract.CommonDataKinds.Phone.CONTACT_ID;
        String NUMBER = ContactsContract.CommonDataKinds.Phone.NUMBER;//0
        String PHONE_TYPE = ContactsContract.CommonDataKinds.Phone.TYPE;

        String [] PROJECTION_PHONE={NUMBER};

        ContentResolver contentResolver = getContentResolver();
        Cursor cursor = contentResolver.query(CONTENT_URI, PROJECTION_EMAIL,ContactsContract.CommonDataKinds.Email.HAS_PHONE_NUMBER + " !=0", null, Phone_CONTACT_ID);

        String phoneNumber;

        HashMap<String,String> hashMap= new HashMap<>();

        while (cursor.moveToNext()){

            String email_contact_id=cursor.getString(0);
            String email=cursor.getString(1);
            try{
                if (hashMap.containsKey(email_contact_id)){
                    String savedString =hashMap.get(email_contact_id);
                    String appended=savedString+" \n "+email;
                    hashMap.put(email_contact_id,appended);
                    continue;
                }
                else{
                    hashMap.put(email_contact_id,"\n "+email);
                }

            }
            catch (Exception e){
                e.printStackTrace();
            }

            String name=cursor.getString(2);
            String lastContact=cursor.getString(3);
            String photo_uri=cursor.getString(4);
            String photo_uri_thumb=cursor.getString(5);

            DatabaseUtils.dumpCurrentRow(cursor);

            phoneNumber=null;
            String type=null;
            StringBuilder sb= new StringBuilder();
            //This is to read multiple phone numbers associated with the same contact
            Cursor phoneCursor = contentResolver.query(PhoneCONTENT_URI, PROJECTION_PHONE, Phone_CONTACT_ID + " = ?", new String[] { email_contact_id }, null);

            while (phoneCursor.moveToNext()) {
                phoneNumber = phoneCursor.getString(0);
                //type = phoneCursor.getString(phoneCursor.getColumnIndex(PHONE_TYPE));
                //DatabaseUtils.dumpCurrentRow(phoneCursor);
                sb.append(" \n ");
               /* String type_text=" -null- ";
                switch (Integer.parseInt(type)){
                    case ContactsContract.CommonDataKinds.Phone.TYPE_HOME:
                        System.out.println("home");
                        type_text="home";
                        break;
                    case ContactsContract.CommonDataKinds.Phone.TYPE_WORK:
                        System.out.println("work");
                        type_text="work";
                        break;
                    case ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE:
                        System.out.println("mobile");
                        type_text="mobile";
                        break;
                    default:
                        break;
                }*/
                sb.append(phoneNumber);
                //output.append("\n Phone number:" + phoneNumber);
            }

            Contact contact= new Contact();
            contact.setName(name);
            contact.setId(email_contact_id);
            contact.setEmail(email);
            contact.setLastContactTime(lastContact);
            contact.setMobile(sb.toString());
            if (photo_uri!=null){
                contact.setPhoto_url(Uri.parse(photo_uri));
            }
            if(photo_uri_thumb!=null){
                contact.setPhoto_url_thumb(Uri.parse(photo_uri_thumb));
            }
            list.add(contact);
            //mrAdapter.notifyDataSetChanged();
            //System.out.println(sb.toString());
        }
        List<Contact> listFinal=new ArrayList<>();
        //System.out.println(hashMap.toString());
        for (Contact contact:list){
            String emailId=contact.getId();
            if(hashMap.containsKey(emailId)){
                String emailString=hashMap.get(emailId);
                contact.setEmail(emailString);
                //System.out.println(emailString);
                listFinal.add(contact);
            }
            else{
                //System.out.println("no key "+emailId);
            }
        }

        return listFinal;
    }
    class asyncTask extends AsyncTask<String,Void, List<Contact>> {
        @Override
        protected void onPreExecute() {
            busy=true;
            feed_list.clear();
            mrAdapter.notifyDataSetChanged();
            progressBar.setVisibility(View.VISIBLE);
            //pBar.setVisibility(View.VISIBLE);
            //swipeRefreshLayout.setRefreshing(true);
        }
        protected List<Contact> doInBackground(String... xx){
            //List<Contact> list= new ArrayList<Contact>();

            //list=test51();

            return test51();
        }
        @Override
        protected void onPostExecute(List<Contact> list) {
            progressBar.setVisibility(View.INVISIBLE);

            feed_list.addAll(list);
            mrAdapter.notifyDataSetChanged();
            busy=false;
        }
        //swipeRefreshLayout.setRefreshing(false);
    }
    void go() {
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_CONTACTS)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_CONTACTS)) {
                Toast.makeText(getBaseContext(), "Need permission to read contacts!", Toast.LENGTH_SHORT).show();
                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_CONTACTS},
                        100);

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_CONTACTS},
                        100);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
        else{
            if (!busy){
                if (checklogPermission()){
                    new asyncTask2().execute();
                }
            }
        }
    }
    boolean checklogPermission(){
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_CALL_LOG)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_CALL_LOG)) {
                Toast.makeText(getBaseContext(), "Need permission to read log!", Toast.LENGTH_SHORT).show();
                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_CALL_LOG},
                        101);
                return false;



            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_CALL_LOG},
                        101);
                return false;
                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
        else{
            return true;
        }
    }
    boolean checkContactPermission(){
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_CALL_LOG)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_CALL_LOG)) {
                Toast.makeText(getBaseContext(), "Need permission to read log!", Toast.LENGTH_SHORT).show();
                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_CALL_LOG},
                        100);
                return false;



            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_CALL_LOG},
                        101);
                return false;
                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
        else{
            return true;
        }
    }
    void go2(){
        if(checkContactPermission()&&checklogPermission()){
            new asyncTask2().execute();
        }
    }
    List<Contact> test51_xphase(){
        Log.d("ss","ddfds");
        List<Contact> list=new ArrayList<Contact>();

        Uri PHONE_CONTENT_URI = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        String PHONE_CONTACT_ID = ContactsContract.CommonDataKinds.Phone.CONTACT_ID;//0
        String NUMBER = ContactsContract.CommonDataKinds.Phone.NUMBER;//1
        String Name = ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME;//2
        String thumb = ContactsContract.CommonDataKinds.Phone.PHOTO_THUMBNAIL_URI;//3
        String lastContactTime = ContactsContract.CommonDataKinds.Phone.LAST_TIME_CONTACTED;//4


        String [] PROJECTION_PHONE={PHONE_CONTACT_ID,NUMBER,Name,thumb,lastContactTime};
        ContentResolver contentResolver = getContentResolver();
        Cursor cursor = contentResolver.query(PHONE_CONTENT_URI, PROJECTION_PHONE,ContactsContract.CommonDataKinds.Phone.HAS_PHONE_NUMBER + " !=0", null, null);

        //log
        Uri LOG_CONTENT_URI = CallLog.Calls.CONTENT_URI;
        String LOG_CONTACT_ID = CallLog.Calls._ID;//0
        String LOG_NUMBER = CallLog.Calls.NUMBER;//1
        String LOG_CALL_TYPE=CallLog.Calls.TYPE;//2
        int LOG_CALL_TYPE_OUT=CallLog.Calls.OUTGOING_TYPE;

        String LOG_DURATION = CallLog.Calls.DURATION;//3


        String [] PROJECTION_LOG={LOG_CONTACT_ID,LOG_NUMBER,LOG_CALL_TYPE,LOG_DURATION};

        HashMap<String,String> hashMap= new HashMap<>();

        while (cursor.moveToNext()){
            //DatabaseUtils.dumpCurrentRow(cursor); //phone cursor
            String id=cursor.getString(0);
            String phoneNumber=cursor.getString(1);
            String name=cursor.getString(2);
            String thumbUri=cursor.getString(3);
            String lastTime=cursor.getString(4);

            ContentResolver contentResolver2 = getContentResolver();
            Cursor logCursor = contentResolver2.query(LOG_CONTENT_URI,
                    PROJECTION_LOG,
                    LOG_NUMBER+" ==? ",
                    new String[] { phoneNumber },
                    null);
            long seconds=0;

            int count=logCursor.getCount();
            String birthday=null;
            if (count>0){
                seconds=0;
                while (logCursor.moveToNext()){
                    String log_type=logCursor.getString(2);
                    String duration=logCursor.getString(3);
                    if (Integer.parseInt(log_type)==LOG_CALL_TYPE_OUT){
                        //Log.d("LOG_TYPE",log_type);
                        if(Long.parseLong(duration)>0){
                            long _duration=Long.parseLong(duration);
                            seconds=seconds+_duration;

                            ContentResolver bd = getContentResolver();
                            Cursor bdc = bd.query(ContactsContract.Data.CONTENT_URI,
                                    new String[] { Event.DATA },
                                    ContactsContract.Data.CONTACT_ID+" = "+id+" AND "+ ContactsContract.Contacts.Data.MIMETYPE+" = '"+Event.CONTENT_ITEM_TYPE+"' AND "+Event.TYPE+" = "+Event.TYPE_BIRTHDAY,
                                    null,
                                    null);
                            if (bdc.getCount() > 0) {
                                while (bdc.moveToNext()) {

                                    birthday = bdc.getString(0);

                                    DatabaseUtils.dumpCurrentRow(bdc);
                                    //DatabaseUtils.dumpCurrentRow(cursor);
                                    //DatabaseUtils.dumpCurrentRow(logCursor);
                                    // now "id" is the user's unique ID, "name" is his full name and "birthday" is the date and time of his birth
                                }
                            }


                        }
                    }
                }
                logCursor.close();

                if (seconds>0){
                    Contact contact= new Contact();
                    contact.setName(name);
                    contact.setMobile(phoneNumber);
                    contact.setPhoto_url_thumb(Uri.parse(thumb));
                    contact.setLastContactTime(lastTime);
                    contact.setDuration(seconds);
                    contact.setDob(birthday);
                    long factor;
                    if(birthday!=null){
                        factor=seconds*(1+1);
                    }
                    else{
                        factor=seconds*(1+0);
                    }
                    contact.setSortFactor(factor);

                    list.add(contact);

                    Log.d("name",""+name);
                    Log.d("total seconds",""+seconds);
                }

            }
        }
        cursor.close();
        Collections.sort(list, new Comparator<Contact>() {
            @Override
            public int compare(Contact contact, Contact t1) {
                long one=contact.getSortFactor();
                long two=t1.getSortFactor();

                return  (int) (two-one);

            }
        });
        return list;


    }
    class asyncTask2 extends AsyncTask<String,Void, List<Contact>> {
        @Override
        protected void onPreExecute() {
            busy=true;
            feed_list.clear();
            mrAdapter.notifyDataSetChanged();
            progressBar.setVisibility(View.VISIBLE);
        }
        protected List<Contact> doInBackground(String... xx){
            //List<Contact> list= new ArrayList<Contact>();

            ;

            return test51_xphase();
        }
        @Override
        protected void onPostExecute(List <Contact> list) {
            progressBar.setVisibility(View.INVISIBLE);

            feed_list.addAll(list);
            mrAdapter.notifyDataSetChanged();
            busy=false;
        }
        //swipeRefreshLayout.setRefreshing(false);
    }
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 100: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    hasContactPermission=true;

                } else {
                    hasContactPermission=false;
                    Toast.makeText(getBaseContext(), "No permission to read Contacts!!", Toast.LENGTH_SHORT).show();
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                break;
            }
            case 101: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    hasLogPermission=true;

                } else {
                    hasLogPermission=false;
                    Toast.makeText(getBaseContext(), "No permission to read log!!", Toast.LENGTH_SHORT).show();
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                break;
            }
            default:
                break;
            // other 'case' lines to check for other
            // permissions this app might request
        }
        Log.d("override funciton","##########");
        if(checkContactPermission() && checklogPermission()){
            new asyncTask2().execute();
        }

    }
}
