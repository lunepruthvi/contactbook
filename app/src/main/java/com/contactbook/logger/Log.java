package com.contactbook.logger;

import android.os.Environment;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;

/**
 * Created by Narayan.Soni on 06-07-2016.
 */
public class Log {

    private static boolean enablelog = true;
    private static boolean enableWrite = false;

    private static final String TAG = "response||";

    public static void print(String response) {
        if (enablelog &&response!=null) {

            android.util.Log.v(TAG, response);
            if (enableWrite) {
                write(new Date()+" "+TAG + " : " + response);
            }
        }
    }

    private static void write(String response) {
        generateNoteOnSD(response);
    }

    static void generateNoteOnSD(String sBody) {
        try {
            File root = new File(Environment.getExternalStorageDirectory(), "Maptags");
            if (!root.exists()) {
                root.mkdirs();
            }
            File gpxfile = new File(root, "log.txt");
            FileWriter writer = new FileWriter(gpxfile, true);
            writer.append(sBody);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {

        }
    }
}
