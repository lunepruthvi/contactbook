package com.contactbook;

import android.net.Uri;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Hetermis on 10/13/2016.
 */

public class Contact {
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    String id;
    String email;
    String name;
    String img;
    String mobile;
    String lastContactTime;

    public long getSortFactor() {
        return sortFactor;
    }

    public void setSortFactor(long sortFactor) {
        this.sortFactor = sortFactor;
    }

    long sortFactor;

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    String dob;

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }

    long duration;
    int sort;

    public Uri getPhoto_url() {
        return photo_url;
    }

    public void setPhoto_url(Uri photo_url) {
        this.photo_url = photo_url;
    }

    public Uri getPhoto_url_thumb() {
        return photo_url_thumb;
    }

    public void setPhoto_url_thumb(Uri photo_url_thumb) {
        this.photo_url_thumb = photo_url_thumb;
    }

    Uri photo_url;
    Uri photo_url_thumb;


    public Contact(){

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getLastContactTime() {
        return lastContactTime;
    }

    public void setLastContactTime(String lastContactTime) {
        //System.out.println(lastContactTime);
        if (lastContactTime==null){

            this.lastContactTime="";
            return;
        }
        if (lastContactTime.compareTo("0")==0){
            this.lastContactTime="";
            return;
        }
        try{
        SimpleDateFormat dt = new SimpleDateFormat("dd MMM, yy hh:mm:ss a");
        Date date=new Date(Long.parseLong(lastContactTime));
        this.lastContactTime = dt.format(date);
        }

        catch (Exception e){
            this.lastContactTime="";
            e.printStackTrace();

        }
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
