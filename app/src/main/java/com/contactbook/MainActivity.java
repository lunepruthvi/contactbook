package com.contactbook;

import android.Manifest;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.CallLog;
import android.provider.ContactsContract;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ProgressBar;
import android.widget.Toast;


import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import android.provider.ContactsContract.CommonDataKinds.Event;

public class MainActivity extends AppCompatActivity {
    RecyclerView mRecyclerView;
    List<Contact> feed_list= new ArrayList<Contact>();
    private RecyclerView.Adapter mrAdapter;
    private ProgressBar progressBar;
    boolean busy=false;
    boolean hasLogPermission=false;
    boolean hasContactPermission=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mRecyclerView = (RecyclerView) findViewById(R.id.rv_contact);
        progressBar= (ProgressBar) findViewById(R.id.progressBar);

        //StaggeredGridLayoutManager staggeredGrid= new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);

        LinearLayoutManager llm= new LinearLayoutManager(getBaseContext());
        mRecyclerView.setLayoutManager(llm);
        mrAdapter = new RVadapter(this,feed_list);

        mRecyclerView.setAdapter(mrAdapter);
        mRecyclerView.setHasFixedSize(true);
        SpacesItemDecoration decoration = new SpacesItemDecoration(10);
        mRecyclerView.addItemDecoration(decoration);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                go2();
            }
        });
        go2();

    }

    //@Override
   // public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
   //     getMenuInflater().inflate(R.menu.menu_main, menu);
    //    return true;
    //}

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    void go2(){
        if (!busy){
            if(checkContactandLogPermissions()){
                new asyncTask2().execute();}

        }
    }
    boolean checklogPermission(){
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_CALL_LOG)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_CALL_LOG)) {
                Toast.makeText(getBaseContext(), "Need permission to read log!", Toast.LENGTH_SHORT).show();
                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_CALL_LOG},
                        101);
                return false;



            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_CALL_LOG},
                        101);
                return false;
                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
        else{
            return true;
        }
    }
    boolean checkContactPermission(){
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_CONTACTS)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_CONTACTS)) {
                Toast.makeText(getBaseContext(), "Need permission to read log!", Toast.LENGTH_SHORT).show();
                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_CONTACTS},
                        100);
                return false;



            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_CONTACTS},
                        100);
                return false;
                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
        else{
            return true;
        }
    }
    boolean checkContactandLogPermissions(){
        boolean c=ContextCompat.checkSelfPermission(this,Manifest.permission.READ_CONTACTS)!= PackageManager.PERMISSION_GRANTED;
        boolean l=ContextCompat.checkSelfPermission(this,Manifest.permission.READ_CALL_LOG)!= PackageManager.PERMISSION_GRANTED;

        if (c||l) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_CONTACTS,Manifest.permission.READ_CALL_LOG},
                        102);
                return false;
        }
        else{
            return true;
        }
    }

    List<Contact> test51_xphase(){
        List<Contact> list=new ArrayList<Contact>();

        Uri PHONE_CONTENT_URI = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        String PHONE_CONTACT_ID = ContactsContract.CommonDataKinds.Phone.CONTACT_ID;//0
        String NUMBER = ContactsContract.CommonDataKinds.Phone.NUMBER;//1
        String Name = ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME;//2
        String thumb = ContactsContract.CommonDataKinds.Phone.PHOTO_THUMBNAIL_URI;//3
        String lastContactTime = ContactsContract.CommonDataKinds.Phone.LAST_TIME_CONTACTED;//4


        String [] PROJECTION_PHONE={PHONE_CONTACT_ID,NUMBER,Name,thumb,lastContactTime};
        ContentResolver contentResolver = getContentResolver();
        Cursor cursor = contentResolver.query(PHONE_CONTENT_URI, PROJECTION_PHONE,ContactsContract.CommonDataKinds.Phone.HAS_PHONE_NUMBER + " !=0", null, null);

        //log
        Uri LOG_CONTENT_URI = CallLog.Calls.CONTENT_URI;
        String LOG_CONTACT_ID = CallLog.Calls._ID;//0
        String LOG_NUMBER = CallLog.Calls.NUMBER;//1
        String LOG_CALL_TYPE=CallLog.Calls.TYPE;//2
        int LOG_CALL_TYPE_OUT=CallLog.Calls.OUTGOING_TYPE;

        String LOG_DURATION = CallLog.Calls.DURATION;//3


        String [] PROJECTION_LOG={LOG_CONTACT_ID,LOG_NUMBER,LOG_CALL_TYPE,LOG_DURATION};

        HashMap<String,String> hashMap= new HashMap<>();

        while (cursor.moveToNext()){
            //DatabaseUtils.dumpCurrentRow(cursor); //phone cursor
            String id=cursor.getString(0);
            String phoneNumber=cursor.getString(1);
            String name=cursor.getString(2);
            String thumbUri=cursor.getString(3);
            String lastTime=cursor.getString(4);

            ContentResolver contentResolver2 = getContentResolver();
            Cursor logCursor = contentResolver2.query(LOG_CONTENT_URI,
                    PROJECTION_LOG,
                    LOG_NUMBER+" ==? ",
                    new String[] { phoneNumber },
                    null);
            long seconds=0;

            int count=logCursor.getCount();
            String birthday=null;
            if (count>0){
                seconds=0;
                while (logCursor.moveToNext()){
                    String log_type=logCursor.getString(2);
                    String duration=logCursor.getString(3);
                    if (Integer.parseInt(log_type)==LOG_CALL_TYPE_OUT){
                        //Log.d("LOG_TYPE",log_type);
                        if(Long.parseLong(duration)>0){
                            long _duration=Long.parseLong(duration);
                            seconds=seconds+_duration;

                            ContentResolver bd = getContentResolver();
                            Cursor bdc = bd.query(android.provider.ContactsContract.Data.CONTENT_URI,
                                    new String[] { ContactsContract.CommonDataKinds.Event.DATA },
                                    android.provider.ContactsContract.Data.CONTACT_ID+" = "+id+" AND "+ ContactsContract.Contacts.Data.MIMETYPE+" = '"+Event.CONTENT_ITEM_TYPE+"' AND "+Event.TYPE+" = "+Event.TYPE_BIRTHDAY,
                                    null,
                                    null);
                            if (bdc.getCount() > 0) {
                                while (bdc.moveToNext()) {

                                    birthday = bdc.getString(0);

                                    DatabaseUtils.dumpCurrentRow(bdc);
                                    //DatabaseUtils.dumpCurrentRow(cursor);
                                    //DatabaseUtils.dumpCurrentRow(logCursor);
                                    // now "id" is the user's unique ID, "name" is his full name and "birthday" is the date and time of his birth
                                }
                            }
                            bdc.close();


                        }
                    }
                }


                if (seconds>0){
                    Contact contact= new Contact();
                    contact.setName(name);
                    contact.setMobile(phoneNumber);
                    contact.setPhoto_url_thumb(Uri.parse(thumb));
                    contact.setLastContactTime(lastTime);
                    contact.setDuration(seconds);
                    contact.setDob(birthday);
                    long factor;
                    if(birthday!=null){
                        factor=seconds*(1+1);
                    }
                    else{
                        factor=seconds*(1+0);
                    }
                    contact.setSortFactor(factor);

                    list.add(contact);

                    Log.d("name",""+name);
                    Log.d("total seconds",""+seconds);
                }
            }
            logCursor.close();
        }
        cursor.close();
        Collections.sort(list, new Comparator<Contact>() {
            @Override
            public int compare(Contact contact, Contact t1) {
                long one=contact.getSortFactor();
                long two=t1.getSortFactor();

                return  (int) (two-one);

            }
        });
        return list;


    }
    class asyncTask2 extends AsyncTask<String,Void, List<Contact>> {
        @Override
        protected void onPreExecute() {
            busy=true;
            feed_list.clear();
            mrAdapter.notifyDataSetChanged();
            progressBar.setVisibility(View.VISIBLE);
        }
        protected List<Contact> doInBackground(String... xx){
            //List<Contact> list= new ArrayList<Contact>();

            ;

            return test51_xphase();
        }
        @Override
        protected void onPostExecute(List <Contact> list) {
            progressBar.setVisibility(View.INVISIBLE);

            feed_list.addAll(list);
            mrAdapter.notifyDataSetChanged();
            busy=false;
        }
        //swipeRefreshLayout.setRefreshing(false);
    }
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 100: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    hasContactPermission=true;

                } else {
                    hasContactPermission=false;
                    Toast.makeText(getBaseContext(), "No permission to read Contacts!!", Toast.LENGTH_SHORT).show();
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                break;
            }
            case 101: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    hasLogPermission=true;

                } else {
                    hasLogPermission=false;
                    Toast.makeText(getBaseContext(), "No permission to read log!!", Toast.LENGTH_SHORT).show();
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                break;
            }
            case 102: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1]==PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                    new asyncTask2().execute();
                    return;

                } else {

                    Toast.makeText(getBaseContext(), "No permission for contacts or logs!!", Toast.LENGTH_SHORT).show();
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                break;
            }
            default:
                break;
            // other 'case' lines to check for other
            // permissions this app might request
        }
        Log.d("fakkflja","fjakjfla");

    }
}
