package com.contactbook;

import android.app.Activity;
import android.content.ContentUris;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.contactbook.logger.Log;

import java.util.ArrayList;
import java.util.List;

public class RVadapter extends RecyclerView.Adapter<RVadapter.ViewHolder> {
  private ArrayList<String> mDataset;
  public List<Contact> datas;
  Activity activity;
  public RVadapter(Activity activity, List<Contact> objects) {
	    datas = objects;
	    this.activity = activity;
	  }
  // Provide a reference to the views for each data item
  // Complex data items may need more than one view per item, and
  // you provide access to all the views for a data item in a view holder
  public class ViewHolder extends RecyclerView.ViewHolder implements OnClickListener{
    // each data item is just a string in this case

      ImageView _profileImage;
      TextView _email;
      TextView _name;
      TextView _phoneNumber;
      TextView _lastContactTime;
      TextView _dob;

    public ViewHolder(View v) {
		super(v);

		_profileImage = (ImageView) v.findViewById(R.id.iv_contact_image);
        _email = (TextView) v.findViewById(R.id.tv_email);
        _name = (TextView) v.findViewById(R.id.tv_contact_name);
        _phoneNumber = (TextView) v.findViewById(R.id.tv_phone_number);
        _lastContactTime = (TextView) v.findViewById(R.id.tv_last_contact_time);
        _dob=(TextView) v.findViewById(R.id.tv_dob);

    }
    @Override
    public void onClick(View v) {

        if (v.getId()==_email.getId()||v.getId() == _profileImage.getId()) {
        }
    }
  }
  // Provide a suitable constructor (depends on the kind of dataset)
  // Create new views (invoked by the layout manager)
  @Override
  public ViewHolder onCreateViewHolder(ViewGroup parent,
      int viewType) {
    // create a new view
    View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item, parent, false);
    // set the view's size, margins, paddings and layout parameters
    ViewHolder vh = new ViewHolder(v);
    return vh;
  }

  // Replace the contents of a view (invoked by the layout manager)
  @Override
  public void onBindViewHolder(ViewHolder holder, int position) {
    // - get element from your dataset at this position
    // - replace the contents of the view with that element

      Contact data = datas.get(position);
      //holder.image.setImageBitmap(data.bitmap_image);
      holder._email.setText(data.getDuration()+" s");

      //Uri person = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, Long.parseLong(data.getId()));
      //Log.print(person.toString());
      //Uri imgUri=Uri.withAppendedPath(person, ContactsContract.Contacts.Photo.CONTENT_DIRECTORY);
      //holder._profileImage.setImageURI(imgUri);
      //Log.print(imgUri.toString());
      //if(data.getPhoto_url()!=null) {
      //    holder._profileImage.setImageURI(data.getPhoto_url());
      //    Log.print("hd");
      //}
      //Log.print(data.getName());

      if(data.getPhoto_url_thumb()!=null){
          holder._profileImage.setImageURI(data.getPhoto_url_thumb());
          //Log.print("thumb");
          //Log.print(data.getPhoto_url_thumb().toString());
      }
      if(data.getPhoto_url_thumb()==null){
            //Log.print("uri null");
          holder._profileImage.setImageResource(R.mipmap.ic_perm_contact_calendar_black_24dp);
      }
      holder._name.setText(data.getName());
      holder._phoneNumber.setText(data.getMobile());
      holder._lastContactTime.setText(data.getLastContactTime());
      holder._dob.setText(data.getDob());
      //Log.print(data.getEmail());
      //Picasso.with(activity).load(data.profileImage).error(R.drawable.ic_by_genre).into(holder.profileImage);
  }

  // Return the size of your dataset (invoked by the layout manager)
  @Override
  public int getItemCount() {
    return datas.size();
  }
}